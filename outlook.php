<style type="text/css">
	body {
		padding-bottom: 300px;
	}
	h1.nombre {
		background: #ff5f3c;
		color: #fff;
		font-family: helvetica, arial;
		text-align: center;
		border-radius: 4px;
		padding: 8px;
		padding-top: 15px;
		margin-bottom: 40px;
		opacity: .2;
	}
	.cont:hover h1.nombre {
		opacity: 1;
	}
	textarea {
		width: 100%;
		height: 1px;
		padding: 10px 0 0 30px;
	}
	.firma {
		padding-bottom: 30px;
	}
	.firma + textarea {
		opacity: 0;
		transition: all .7s;
	}
	.firma:hover + textarea, textarea:hover {
		opacity: 1;
		height: 200px;
	}
	textarea + p {
		margin: 0;
		opacity: 0;
		font-style: italic;
		font-size: .8em;
		transition: all .2s; 
	}
	textarea:hover + p {
		opacity: .7;
	}
</style>
<div style="width:600px;">
<?php

$firmas = file_get_contents('firmas.txt');

$firmas = explode("\n", $firmas);

foreach ($firmas as $n => $v) {

	switch ($n%5) {
		case 0:
			$nombre = $v;
			break;
		
		case 1:
			$cargo = $v;
			break;
		
		case 2:
			$email = $v;
			break;
		
		case 3:
			$fono = $v;
			break;
		
		default:
			?>
<div class="cont">
<h1 class="nombre"><?php echo $nombre;?></h1>
<div class="firma"><link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
<table border="0" style="color: #FF5F3C; font-family: montserrat, sans-serif, arial; font-size: 13px;">
	<tr>

		<td>
			<img src="http://firmas.trapemn.cl/firma-trapemn.png" width="188" />
		</td>
		<td>
			<div style="margin-right: 3px">&nbsp;</div>
		</td>
		<td>
			<h2 style="margin: -4px 0 -5px;"><?php echo $nombre;?></h2>
			<p style="margin:5px 0 -8px"><?php echo $cargo;?> / <a href="mailto:<?php echo $email;?>" style="text-decoration: none; color: #FF5F3C; white-space: nowrap;"><?php echo $email;?></a></p>
			<p style="white-space: nowrap;font-size: 9pt;">
				<img src="http://firmas.trapemn.cl/iconos-firma.png" width="70" style="vertical-align: middle;" /> <small style="margin-left: 10px;">Concepción - Santiago - Lima / <strong><a href="http://www.trapemn.tv" style="text-decoration: none; color: #FF5F3C;">www.trapemn.tv</a></strong></small>
			</p>
		</td>
	</tr>
</table></div>
</div>
			<?php
			break;
	}
}

?>
</div>

<script type="text/javascript" src="jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$('.firma').each(function(index, el) {
		var firma = $(el),
			html = firma.html();
		firma.after($('<textarea>').text(html).on('click',function(){
			$(this).select();
			document.execCommand('copy');
			alert('Firma de "' + firma.find('h2').first().text() + '" copiada.')
		}));
	});
	$('textarea').after('<p>(Clic en el texto para copiar)')
</script>
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>